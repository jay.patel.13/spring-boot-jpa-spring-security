package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UsersDAO;
import com.example.demo.entities.Users;
import com.example.demo.service.UsersService;


@Service
public class UsersServiceImpl implements UsersService{

	@Autowired
	private UsersDAO userRepository;

	@Override
	public long createUser(Users user) {
		Users u = this.userRepository.save(user);
		return u.getId();
	}
}
