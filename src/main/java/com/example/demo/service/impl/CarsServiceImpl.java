package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.CarsDAO;
import com.example.demo.dto.CarsDTO;
import com.example.demo.entities.Cars;
import com.example.demo.service.CarsService;

@Service
public class CarsServiceImpl implements CarsService {

	@Autowired
	private CarsDAO carRepository;
	
	public long createCar(Cars car) {
		Cars u = this.carRepository.save(car);
		return u.getId();
	}

	@Override
	public String updateCar(Cars car) {
		
		Cars c1 = this.carRepository.findOneByCarId(car.getCarId());
		if (c1 != null) {
			if (c1.getId() == car.getId()) {
				BeanUtils.copyProperties(car, c1);
				Cars u = this.carRepository.save(c1);
				return "car id " + u.getCarId() + " successfully updated";
			}
			return "car id is not matched";
		} else {
			return "car id is not available";
		}
		
	}

	@Override
	public String deleteCar(String id) {
		// TODO Auto-generated method stub
		List<Cars> cars = this.carRepository.findAllByCarId(id);
		if(cars.isEmpty()) {
			return "This car id is not available";
		}else {
			cars.stream().forEach(car -> {
				this.carRepository.deleteById(car.getId());
			});		
			return "Deleted successfully";
		}
	}

	@Override
	public List<CarsDTO> findAllCars() {
		// TODO Auto-generated method stub
		List<CarsDTO> carDTOs = new ArrayList<>();
		List<Cars> cars = this.carRepository.findAll();
		cars.stream().forEach(car -> {
			CarsDTO carDTO = new CarsDTO();
			BeanUtils.copyProperties(car, carDTO);
			carDTOs.add(carDTO);
		});
		return carDTOs;
		
	}
	
	
}
