package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.CarsDTO;
import com.example.demo.entities.Cars;

public interface CarsService {

	String updateCar(Cars car);

	String deleteCar(String id);

	List<CarsDTO> findAllCars();

	long createCar(Cars car);

}
