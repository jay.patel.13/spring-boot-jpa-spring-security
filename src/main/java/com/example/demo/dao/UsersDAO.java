package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Users;


public interface UsersDAO extends JpaRepository<Users, Long> {

	Users findByUserName(String username);
}
