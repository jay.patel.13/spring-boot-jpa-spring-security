package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Cars;

public interface CarsDAO extends JpaRepository<Cars, Long> {

	Cars findOneByCarId(String id);

	List<Cars> findAllByCarId(String id);

}
