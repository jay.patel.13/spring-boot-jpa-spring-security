package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Users;
import com.example.demo.service.UsersService;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private UsersService us;
	
	@PostMapping("/createUser")
	  public long createUser(@RequestBody Users user) throws Exception {
			return this.us.createUser(user);
	  }
}
