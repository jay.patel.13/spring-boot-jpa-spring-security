package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CarsDTO;
import com.example.demo.entities.Cars;
import com.example.demo.entities.Users;
import com.example.demo.service.CarsService;

@RestController
@CrossOrigin
@RequestMapping("/cars")
public class CarsController {
	
	@Autowired
	private CarsService cs;
	
	@PostMapping("/createCar")
	  public long createCar(@RequestBody Cars car) throws Exception {
			return this.cs.createCar(car);
	  }
	
	@PostMapping("/updateCar")
	  public String updateCar(@RequestBody Cars car) throws Exception {
			return this.cs.updateCar(car);
	  }
	
	@DeleteMapping("/deleteCar")
	  public String deleteCar(@RequestParam("carId") String id) throws Exception {
			return this.cs.deleteCar(id);
	  }
	
	@GetMapping("/allCars")
	  public List<CarsDTO> findAllCars() throws Exception {
			return this.cs.findAllCars();
	  }
}
